## UI Task 
This project was made for testing the various Unity UI Skills that are required by any developer. The aim of this project was to create two responsive user interfaces - one portrait and one landscape and test them on various resolutions.

### Landscape Resolutions
* 1920 * 1080
* 940 * 540

### Portrait Resolutions
* 1080 * 1920
* 720 * 1280
* 1242 * 2208
* 540 * 960